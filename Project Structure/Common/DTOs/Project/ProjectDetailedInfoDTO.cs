﻿using Common.DTOs.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Project
{
    public class ProjectDetailedInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int TeamMembersCount { get; set; }

        public override string ToString()
        {
            return $"{Project}\nLongest task by description:\n{LongestTask?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Shortest task by name:\n{ShortestTask?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Team Members Count:\t{TeamMembersCount}\n";
        }
    }
}
