﻿
namespace Common.DTOs.Team
{
    public class TeamUpdateDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
