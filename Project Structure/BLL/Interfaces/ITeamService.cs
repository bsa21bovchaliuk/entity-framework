﻿using Common.DTOs.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetAllAsync();

        Task<TeamDTO> GetByIdAsync(int id);

        Task<TeamDTO> CreateAsync(TeamCreateDTO teamDTO);

        Task<TeamDTO> UpdateAsync(TeamUpdateDTO teamDTO);

        Task DeleteByIdAsync(int id);
    }
}
