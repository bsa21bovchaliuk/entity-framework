﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class UserEntity : BaseEntity
    {
        public UserEntity()
        {
            Tasks = new HashSet<TaskEntity>();
            Projects = new HashSet<ProjectEntity>();
        }

        public int? TeamId { get; set; }
        public virtual TeamEntity Team { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }

        public virtual ICollection<TaskEntity> Tasks { get; private set; }
        public virtual ICollection<ProjectEntity> Projects { get; private set; }
    }
}
